//
//  OurTableViewCell.m
//  TaskTracker
//
//  Created by Fantom on 2/27/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//
#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width/100
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height/100
#define BUTTON_WIDTH SCREEN_WIDTH * 27

#import "OurTableViewCell.h"

@implementation OurTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        _taskPerformerName                 = [UILabel new];
        _taskPerformerName.clipsToBounds   = YES;
        _taskPerformerName.textAlignment   = NSTextAlignmentCenter;
        _taskPerformerName.font            = [UIFont fontWithName:@"ArialRoundedMTBold" size:SCREEN_HEIGHT*3];
        _taskPerformerName.textColor       = [UIColor blackColor];

        _taskText                    = [UILabel new];
        _taskText.clipsToBounds      = YES;
        _taskText.textAlignment      = NSTextAlignmentLeft;
        _taskText.font               = [UIFont fontWithName:@"ArialRoundedMTBold" size:SCREEN_HEIGHT*2.5];
        _taskText.textColor          = [UIColor blackColor];
        _taskText.layer.cornerRadius = 10.0f;
        _taskText.backgroundColor    = [UIColor colorWithWhite:0.902 alpha:1.000];
        
        _statusButton = [StatusButton new];
        _statusButton.buttonState = InqueueState;
        _statusButton.clipsToBounds = YES;
        [_statusButton setTitle:@"In queue" forState:UIControlStateNormal];
        [_statusButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_statusButton.titleLabel setFont:[UIFont fontWithName:@"ArialRoundedMTBold" size:SCREEN_HEIGHT*3]];
        [_statusButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _statusButton.titleLabel.numberOfLines = 2;
        _statusButton.layer.cornerRadius = 10.0f;
        [_statusButton setBackgroundColor:[UIColor colorWithRed:0.337 green:0.529 blue:0.745 alpha:1.000]];


        self.selectionStyle = UITableViewCellSelectionStyleNone;

        [self addSubview:_taskPerformerName];
        [self addSubview:_taskText];
        [self addSubview:_statusButton];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _taskPerformerName.frame = CGRectMake(SCREEN_WIDTH * 2, 0, 0, 0);
    [_taskPerformerName sizeToFit];
    
    _taskText.frame = CGRectMake(SCREEN_WIDTH * 2,
                                 _taskPerformerName.frame.size.height + 1,
                                 self.frame.size.width - BUTTON_WIDTH - SCREEN_WIDTH * 2,
                                 self.frame.size.height - _taskPerformerName.frame.size.height);
    _statusButton.frame = CGRectMake(self.frame.size.width - BUTTON_WIDTH, 0, BUTTON_WIDTH, self.frame.size.height);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
