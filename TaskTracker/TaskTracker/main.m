//
//  main.m
//  TaskTracker
//
//  Created by Fantom on 2/27/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
