//
//  AppDelegate.m
//  TaskTracker
//
//  Created by Fantom on 2/27/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import "AppDelegate.h"
#import "OurTableViewController.h"

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    _window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[OurTableViewController new]];
    
    navigationController.navigationBar.barTintColor = [UIColor lightGrayColor];
    navigationController.navigationBar.barStyle  = UIBarStyleBlackOpaque;
    navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    navigationController.toolbar.barTintColor = [UIColor lightGrayColor];
    navigationController.toolbar.barStyle  = UIBarStyleBlackOpaque;
    navigationController.toolbar.tintColor = [UIColor whiteColor];
    navigationController.toolbarHidden = NO;
    
    self.window.rootViewController = navigationController;
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
