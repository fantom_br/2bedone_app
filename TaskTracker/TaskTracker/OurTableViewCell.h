//
//  OurTableViewCell.h
//  TaskTracker
//
//  Created by Fantom on 2/27/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StatusButton.h"

@interface OurTableViewCell : UITableViewCell

@property (nonatomic) UILabel *taskPerformerName;
@property (nonatomic) UILabel *taskText;
@property (nonatomic) StatusButton *statusButton;

@end
