//
//  StatusButton.m
//  TaskTracker
//
//  Created by Fantom on 3/1/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//
#import "StatusButton.h"

@implementation StatusButton {
    NSDictionary *dict;
    StatusButton *tempButton;
}

- (id)init {
    self = [super init];
    if (self) {
        dict = @{[NSNumber numberWithInt:InqueueState]:     @"Change status to In progress?",
                 [NSNumber numberWithInt:InProcessState]:   @"Change status to Done?",
                 [NSNumber numberWithInt:DoneSate]:         @"Change status to In queque?",
                };
    }
    
    return self;
}

- (void)changeState:(StatusButton *)button {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:dict[[NSNumber numberWithInt:(int)button.buttonState]]
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Ok", nil];
    
    [alertView show];
    tempButton = button;
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex) {
        if (tempButton.buttonState == InqueueState) {
            tempButton.buttonState = InProcessState;
            [tempButton setTitle:@"In Progress" forState:UIControlStateNormal];
            [tempButton setBackgroundColor:[UIColor colorWithRed:0.949 green:0.753 blue:0.176 alpha:1.000]];
        } else if (tempButton.buttonState == InProcessState) {
            tempButton.buttonState = DoneSate;
            [tempButton setTitle:@"Done" forState:UIControlStateNormal];
            [tempButton setBackgroundColor:[UIColor colorWithRed:0.451 green:0.957 blue:0.357 alpha:1.000]];
        }else if (tempButton.buttonState == DoneSate) {
            tempButton.buttonState = InqueueState;
            [tempButton setTitle:@"In queue" forState:UIControlStateNormal];
            [tempButton setBackgroundColor:[UIColor colorWithRed:0.337 green:0.529 blue:0.745 alpha:1.000]];
        }
    }
}

@end