//
//  ViewController.h
//  TaskTracker
//
//  Created by Fantom on 2/27/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StatusButton.h"

@interface OurTableViewController : UITableViewController

@property (nonatomic) NSMutableArray *taskPerformer;
@property (nonatomic) NSMutableArray *task;
@property (nonatomic) NSMutableArray *timeLeft;

- (void)changeStatus:(StatusButton *)button;

@end

