//
//  ViewController.m
//  TaskTracker
//
//  Created by Fantom on 2/27/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//
#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width/100
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height/100

#import "OurTableViewController.h"
#import "OurTableViewCell.h"


@implementation OurTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UILabel *navBarLabel        = [[UILabel alloc] initWithFrame:CGRectZero];
    navBarLabel.backgroundColor = [UIColor clearColor];
    navBarLabel.font            = [UIFont fontWithName:@"ArialRoundedMTBold" size:SCREEN_HEIGHT*3.5f];
    navBarLabel.shadowColor     = [UIColor colorWithWhite:0.0 alpha:0.5];
    navBarLabel.shadowOffset    = CGSizeMake(0.0f, 1.0f);
    navBarLabel.textAlignment   = NSTextAlignmentCenter;
    navBarLabel.textColor       = [UIColor whiteColor];
    navBarLabel.text            = @"Tasks to do";
    [navBarLabel sizeToFit];
    self.navigationItem.titleView = navBarLabel;
    
    UIBarButtonItem *buttonItem = [[ UIBarButtonItem alloc ] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                  target: self
                                                                                  action: nil ];
    self.toolbarItems = @[buttonItem];
    
    if (self.navigationController.viewControllers.count != 1) {
        return;
    }
    
    self.tableView.bounces = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    _taskPerformer = [[NSMutableArray alloc] initWithObjects:@"Obozhdi", @"Fantom", nil];
    _task          = [[NSMutableArray alloc] initWithObjects:@"Finish home task",@"Just fack off", nil];
    _timeLeft      = [[NSMutableArray alloc] initWithObjects:@"1 h 55 m",@"3 h 54 m", nil];
    
    [self.tableView registerClass:[OurTableViewCell class] forCellReuseIdentifier:@"OurTableViewCell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _taskPerformer.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"OurTableViewCell";
    OurTableViewCell *menucell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                                  forIndexPath:indexPath];
    menucell.taskPerformerName.text = [NSString stringWithFormat:@"%@", _taskPerformer[indexPath.row]];
    menucell.taskText.text          = [NSString stringWithFormat:@"%@", _task[indexPath.row]];
    [menucell.statusButton addTarget:self action:@selector(changeStatus:) forControlEvents:UIControlEventTouchUpInside];
    
    return menucell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return SCREEN_HEIGHT*10.5f;
}

- (void)changeStatus:(StatusButton *)button {
    [button changeState:button];
}

@end
