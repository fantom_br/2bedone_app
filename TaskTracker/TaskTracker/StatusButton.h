//
//  StatusButton.h
//  TaskTracker
//
//  Created by Fantom on 3/1/15.
//  Copyright (c) 2015 Fantom. All rights reserved.
//

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width/100
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height/100

#import <UIKit/UIKit.h>

typedef enum {
    InqueueState,
    InProcessState,
    DoneSate,
}ButtonState;


@interface StatusButton : UIButton

@property (nonatomic) ButtonState buttonState;

- (void)changeState:(StatusButton *)button;

@end


